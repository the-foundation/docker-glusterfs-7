FROM ubuntu:focal

MAINTAINER No Body
RUN apt-get update &&  apt-get install -y python3-software-properties software-properties-common iproute2 iputils-ping && add-apt-repository ppa:gluster/glusterfs-7 && apt-get update && apt-get install -y glusterfs-server supervisor && apt-get clean && find /var/lib/apt/lists/ -type f -delete
RUN tar cvzf /etc/gluster.init.tgz /etc/glusterfs
    
RUN mkdir -p /var/log/supervisor

VOLUME ["/gluster_bricks"]

RUN mkdir -p /usr/local/bin
ADD ./bin /usr/local/bin
RUN chmod +x /usr/local/bin/*.sh
ADD ./etc/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
EXPOSE 24007 24008 49152 38465 38466 38467
CMD ["/usr/local/bin/run.sh"]
