# docker-glusterfs-7


to start it as e.g. gluster2 on a bridge named "glusterfs"
```
docker run --restart=always --name gluster2 -v /bricks:/bricks -v /etc/glusterfs:/etc/glusterfs:z -v /var/lib/glusterd:/var/lib/glusterd:z -v /var/log/glusterfs:/var/log/glusterfs:z -v /sys/fs/cgroup:/sys/fs/cgroup:ro --mount type=bind,source=/gluster_bricks,target=/gluster_bricks,bind-propagation=rshared -d --privileged=true --ip 192.168.123.22 --net=glusterfs -v /dev/:/dev thefoundation/docker-gsluterfs-7
```---


<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/docker-glusterfs-7/README.md/logo.jpg" width="480" height="270"/></div></a>
